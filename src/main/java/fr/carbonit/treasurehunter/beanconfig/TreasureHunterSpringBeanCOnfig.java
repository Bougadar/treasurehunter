package fr.carbonit.treasurehunter.beanconfig;


import fr.carbonit.treasurehunter.metier.aventurier.Aventurier;
import fr.carbonit.treasurehunter.metier.carte.Carte;
import fr.carbonit.treasurehunter.services.AventurierFactory;
import fr.carbonit.treasurehunter.services.CarteFileFactory;
import fr.carbonit.treasurehunter.services.ServiceResultatReport;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import java.util.List;
import java.util.function.Function;

/**
 * Created by bougadar on 29/04/2015.
 */
@Configuration
@EnableAspectJAutoProxy
public class TreasureHunterSpringBeanCOnfig {
	public static final String FICHIER_AVENTURIER = "aventurier";
	public static final String FICHIER_CARTE = "carte";
	public static final String FICHIER_RESULTAT = "resultat";
	public static final String AVENTURIER_FACTORY = "aventurieFactory";
	public static final String CARTE_FACTORY = "carteFactory";
	public static final String SERVICE_REPORT = "serviceReport";

	public TreasureHunterSpringBeanCOnfig() {
	}


	private Function<String, Resource> resource = ClassPathResource::new;


	@Bean
	@Qualifier(value = FICHIER_AVENTURIER)
	public Resource fichierAventurier() {
		return resource.apply("input/carte/aventurier.txt");

	}


	@Bean
	@Qualifier(value = FICHIER_RESULTAT)
	public Resource fichierResultat() {
	 return new PathResource("C:/log/resultat.txt");
	}

	@Bean
	@Qualifier(value = FICHIER_CARTE)
	public Resource fichierCarte() {
		return resource.apply("input/carte/carte.txt");
	}

	@Bean(name = AVENTURIER_FACTORY)
	public List<Aventurier> aventurierList() throws Exception {
		AventurierFactory aventurierFactory = new AventurierFactory(fichierAventurier(), carte());

		return aventurierFactory.getObject();
	}

	@Bean(name = CARTE_FACTORY)
	public Carte carte() throws Exception {
		CarteFileFactory carteFileFactory = new CarteFileFactory(fichierCarte());
		Carte carte = carteFileFactory.getObject();

		return carte;
	}

	@Bean(name = SERVICE_REPORT)
	public ServiceResultatReport serviceResultatReport() {
	 return new ServiceResultatReport(fichierResultat());
	}

}