package fr.carbonit.treasurehunter.services;

import ch.qos.logback.classic.Logger;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.Range;
import fr.carbonit.treasurehunter.beanconfig.TreasureHunterSpringBeanCOnfig;
import fr.carbonit.treasurehunter.metier.carte.Carte;
import fr.carbonit.treasurehunter.metier.carte.Position;
import fr.carbonit.treasurehunter.metier.carte.TypePosition;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by bougadar on 28/04/2015.
 */
@Component
public class CarteFileFactory implements GameElementFactory<Carte> {
 private final Resource fichierATraiter;
	private final Logger logger = (Logger) LoggerFactory.getLogger(CarteFileFactory.class);


 public CarteFileFactory(@Qualifier(TreasureHunterSpringBeanCOnfig.FICHIER_CARTE) Resource fichierATraiter) {
	this.fichierATraiter = fichierATraiter;
 }

	private final Carte genererCarteDeJeux() {
		Carte carte = null;
		try {
			carte = new CarteFileParser().parseMapFile(fichierATraiter.getFile());
		} catch (IOException e) {
			logger.debug(e.getMessage());

		} finally {
			return carte;
		}

 }


	@Override
	public Carte getObject() throws Exception {
		return genererCarteDeJeux();
	}

	@Override
	public Class<?> getObjectType() {
		return Carte.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	private static class CarteFileParser {


	private BiFunction<Integer, Integer, ConcurrentHashMap<String, Position>> constructionDesCasesDeLaCarte = (y, x)->{

	 List<Integer> listeColonne = ContiguousSet.create(Range.closed(1, y.intValue()), DiscreteDomain.integers()).asList();
	 List<Integer> listeLigne = ContiguousSet.create(Range.closed(1, x.intValue()), DiscreteDomain.integers()).asList();
	 ConcurrentHashMap<String, Position> setPosition = new ConcurrentHashMap<>();
	 listeColonne.stream().flatMap(colonne->listeLigne.stream().map(ligne->new Position.Builder().withColonne(colonne).withLigne(ligne).build()))
							 .forEach(p->{
								p.setNatureDeLaPosition(TypePosition.NORMALE);
								setPosition.putIfAbsent(p.toString(), p);});
	 return setPosition;
	};
	private BiConsumer<Map<String, TypePosition>, ConcurrentHashMap<String, Position>> definirLesTypeDeCase = (mapTypeDeCases, positions)->{
	 mapTypeDeCases.entrySet().stream().forEach(type->positions.get(type.getKey()).setNatureDeLaPosition(type.getValue()));


	};

	private Carte parseMapFile(File fichierCarte) throws IOException {

	 List<String> listeLignesFichierCarte = Files.readAllLines(fichierCarte.toPath());
	 Iterator<String> iteratorLigne = listeLignesFichierCarte.stream().iterator();

		ConcurrentHashMap<String, Position> mapPositon = new ConcurrentHashMap<>();
	 boolean carteProceed = false;
	 Carte.Builder carteBuilder = Carte.Builder.carte();


	 while (iteratorLigne.hasNext()) {
		String ligneDefinitionCarte = iteratorLigne.next();


		if (!carteProceed) {

		 String[] dimCarte = ligneDefinitionCarte.split(" ");
		 if (dimCarte[0].equals("C")){
			 mapPositon = constructionDesCasesDeLaCarte.apply(Integer.valueOf(dimCarte[1]), Integer.valueOf(dimCarte[2]));

			carteProceed = true;
			 ligneDefinitionCarte = iteratorLigne.next();
		 }

		}
		Position positionSpeciale = positionsSpecialeCreator.apply(ligneDefinitionCarte);
		 traitementsDesAutresLigne:
		 mapPositon.replace(positionSpeciale.toString(), positionSpeciale, positionSpeciale);


	 }
		return carteBuilder.withCases(mapPositon).build();
	}

		private Function<String, Position> positionsSpecialeCreator = (ligne) -> {
			String[] position = ligne.split(" ");
			String colonne = position[1].split("-")[0];
			String lign = position[1].split("-")[1];
		 Position positionSpeciale = null;
			switch (position[0]) {

				case "M":
				 positionSpeciale = new Position.Builder().withColonne(Integer.valueOf(colonne)).withLigne(Integer.valueOf(lign)).buildMontagne();

					break;

				case "T":
				 positionSpeciale = new Position.Builder().withColonne(Integer.valueOf(colonne)).withLigne(Integer.valueOf(lign)).withTresor(Integer.valueOf(position[2])).buildTresor();


			}
		 return positionSpeciale;
		};
 }
}
