package fr.carbonit.treasurehunter.services;

import org.springframework.beans.factory.FactoryBean;

import java.io.IOException;

/**
 * Created by Sekou on 29/04/2015.
 */
public interface GameElementFactory<T> extends FactoryBean<T> {

}
