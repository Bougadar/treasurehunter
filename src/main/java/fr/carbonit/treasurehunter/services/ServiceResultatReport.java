package fr.carbonit.treasurehunter.services;

import ch.qos.logback.classic.Logger;
import fr.carbonit.treasurehunter.beanconfig.TreasureHunterSpringBeanCOnfig;
import fr.carbonit.treasurehunter.metier.aventurier.Aventurier;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;

import java.io.*;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by bougadar on 01/05/2015.
 */
public class ServiceResultatReport {

	private final static Logger logger = (Logger) LoggerFactory.getLogger(ServiceResultatReport.class);

	private Resource fichierResultat;


	@Autowired
	public ServiceResultatReport(@Qualifier(TreasureHunterSpringBeanCOnfig.FICHIER_RESULTAT) Resource fichierResultat) {
		this.fichierResultat = fichierResultat;
	}

	public void ReportResultatToFile(List<Aventurier> listeDesParticipant) throws IOException {
		try
				(FileWriter fileOutputStream = new FileWriter(fichierResultat.getFile(), true)) {
			BufferedWriter bufferedOutputStream = new BufferedWriter(fileOutputStream);
			listeDesParticipant.stream().sequential().forEach(x -> {
				try {

					bufferedOutputStream.write(x.toString() + System.lineSeparator());
					bufferedOutputStream.flush();


				} catch (Exception e) {
					logger.debug(" erreur lors de l'ecriture des résultat de l'aventurier nommer" + " " + x + "\n" + e.fillInStackTrace());
				}
			});

		}

	}
}
