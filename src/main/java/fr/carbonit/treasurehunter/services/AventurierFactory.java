package fr.carbonit.treasurehunter.services;

import ch.qos.logback.classic.Logger;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.carbonit.treasurehunter.beanconfig.TreasureHunterSpringBeanCOnfig;
import fr.carbonit.treasurehunter.metier.aventurier.Aventurier;
import fr.carbonit.treasurehunter.metier.aventurier.Mouvement;
import fr.carbonit.treasurehunter.metier.aventurier.Orientation;
import fr.carbonit.treasurehunter.metier.carte.Carte;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import static java.util.stream.Collectors.toList;

/**
 * Created by bougadar on 28/04/2015.
 */

public class  AventurierFactory  implements GameElementFactory<List<Aventurier>> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(AventurierFactory.class);

	private static final String NOM = "nom";
	private static final String ORIENTATION = "orientation";
	private static final String MOUVEMENT = "mouvement";
	private static final String POSITION_INITIALE = "positionInitiale";

	private final Resource fichierATraiter;
	private Carte carte;

	public AventurierFactory(@Qualifier(TreasureHunterSpringBeanCOnfig.FICHIER_AVENTURIER) Resource fichierATraiter, Carte carte) {
		this.fichierATraiter = fichierATraiter;
		this.carte = carte;
	}


	private List<Aventurier> aventurierCreator() throws IOException {
		List<Aventurier> listAventurierACreer = Lists.newArrayList();

		List<Map<String, String>> listParametreAventurierACreer = new AventurierFileParser().parseLine(fichierATraiter.getFile());

		listAventurierACreer = Lists.newArrayList(listParametreAventurierACreer.stream().map(x -> createAventurier().apply(x)).collect(toList()));

		return listAventurierACreer;


	}

	private Function<Map<String, String>, Aventurier> createAventurier() {

		return mapParametre->new Aventurier.Builder().withNomAventurier(mapParametre.get(NOM)).
				withOrientationInitiale(Orientation.valueOf(mapParametre.get(ORIENTATION.toString()))).
				withSequenceDeplacements(Mouvement.mouvementBuilder(mapParametre.get(MOUVEMENT))).withPositioninitiale((this.carte.getPositionReferences(mapParametre.get(POSITION_INITIALE)))).build();
	}


	private List<Aventurier> createElement() {
		List<Aventurier> aventuriers = Lists.newArrayList();
		try {

			aventuriers = aventurierCreator();
		} catch (IOException e) {
			logger.debug(e.getMessage());
		}
		this.carte.setAventuriers(aventuriers);
		return aventuriers;
	}

	@Override
	public List<Aventurier> getObject() throws Exception {
		return createElement();
	}

	@Override
	public Class<?> getObjectType() {
		return List.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	private static class AventurierFileParser {

		public AventurierFileParser() {
		}


		public List<Map<String, String>> parseLine(File file) throws IOException {
			List<String> list = Files.readAllLines(file.toPath());
			List<List<String>> listParametreAventurier = Lists.newArrayList();
			list.stream().forEach(x -> listParametreAventurier.add((Arrays.stream(x.trim().split(" ")).collect(toList()))));


			List<Map<String, String>> mapList = listParametreAventurier.stream().map((s) -> {
				String[] pos = s.get(1).split("-");
				String finale = pos[0].concat(" ").concat(pos[1]).toString();
				Map<String, String> map = Maps.newHashMap();
				map.put(NOM, s.get(0));
				map.put(POSITION_INITIALE, finale);
				map.put(ORIENTATION, s.get(2));
				map.put(MOUVEMENT, s.get(3));
				return map;
			}).collect(toList());

			return mapList;
		}

	}
}
