package fr.carbonit.treasurehunter.controller;

import fr.carbonit.treasurehunter.beanconfig.TreasureHunterSpringBeanCOnfig;
import fr.carbonit.treasurehunter.metier.carte.Carte;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by bougadar on 26/04/2015.
 */
public class Main {


	public static void main(String[] args) {
		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(TreasureHunterSpringBeanCOnfig.class);

		Carte carteDeJeux = annotationConfigApplicationContext.getBean(Carte.class);
		carteDeJeux.start();
	}


}
