package fr.carbonit.treasurehunter.metier.carte;

import java.io.Serializable;


import java.io.Serializable;
import java.util.Optional;
import java.util.OptionalLong;

/**
 * Created by bougadar on 26/04/2015.
 */
public class Position implements Serializable, Comparable<Position> {
	private int colonne;
	private int ligne;
	private Optional<Integer> tresor;
	private  TypePosition natureDeLaPosition;

	private EtatPosition etatPosition;


	private Position(Builder builder) {
		colonne = builder.colonne;
		ligne = builder.ligne;


	}

	private Position(Builder builder, TypePosition typePosition) {
		this(builder);
		this.natureDeLaPosition = typePosition;
	}

	private Position(Builder builder, TypePosition typePosition, Optional<Integer> nombreTresor) {
		this(builder, typePosition);
		this.tresor = nombreTresor;
	}



	public int getColonne() {
		return colonne;
	}

	public int getLigne() {
		return ligne;
	}

	public Integer getTresor() {
		return tresor.get();
	}

	@Override
	public int compareTo(Position o) {
		return ((o.colonne - this.colonne) - (o.ligne - this.ligne));
	}


	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Position))
			return false;
		Position position = (Position) o;
		return com.google.common.base.Objects.equal(colonne, position.colonne) && com.google.common.base.Objects.equal(ligne, position.ligne);
	}

	@Override
	public int hashCode() {
		return com.google.common.base.Objects.hashCode(colonne, ligne);
	}


	public static final class Builder {
		private int colonne;
		private int ligne;
		private TypePosition natureDeLaPosition;
		private Optional<Integer> tresor;

		public Builder() {
		}

		public Builder withColonne(int colonne) {
			this.colonne = colonne;
			return this;
		}

		public Builder withLigne(int ligne) {
			this.ligne = ligne;
			return this;
		}

		public Builder withTresor(int nombreTresor) {
			this.tresor = Optional.of(nombreTresor);
			return this;
		}



		public Position build() {
			return new Position(this);
		}

		public Position buildMontagne() {
			return new Position(this, TypePosition.MONTAGNE);
		}

		public Position buildTresor() {
			return new Position(this, TypePosition.TRESOR, tresor);
		}
	}

 public TypePosition getNatureDeLaPosition() {
	 return this.natureDeLaPosition;
 }

 public void setNatureDeLaPosition(final TypePosition natureDeLaPosition) {
	this.natureDeLaPosition = natureDeLaPosition;
 }

 public void setTresor(Optional<Integer> tresor) {
	this.tresor = tresor;
 }

 @Override
 public String toString() {
	 return colonne + " " + ligne;

 }
}
