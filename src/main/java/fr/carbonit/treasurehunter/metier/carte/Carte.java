package fr.carbonit.treasurehunter.metier.carte;

import ch.qos.logback.classic.Logger;
import fr.carbonit.treasurehunter.metier.aventurier.Aventurier;
import fr.carbonit.treasurehunter.services.ServiceResultatReport;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by bougadar on 26/04/2015.
 */
public class Carte {

	private final int dimensionCarte;

	@Autowired
	private ServiceResultatReport serviceResultatReport;

	public static ConcurrentHashMap<String, Position> CASES;


	private List<Aventurier> aventuriers;
	private static Logger logger = (Logger) LoggerFactory.getLogger(Carte.class);

	private Carte(final ConcurrentHashMap<String, Position> positionsDeLaCarte, final List<Aventurier> aventuriers) {
		this.dimensionCarte = positionsDeLaCarte.size();
		this.CASES = positionsDeLaCarte;
		logger.info("La carte de " + this.dimensionCarte + " vient d'être créer");
	}


	public final static Position getPositionReferences(final String coordonnee) {
		Position position = CASES.get(coordonnee);
		return position;
	}
	
	public void start() {
		logger.info("Début de la partie");
		this.aventuriers.stream().sequential().forEach(aventurier -> aventurier.executerDeplacement());
		logger.info("Fin de la partie");
		try {
			serviceResultatReport.ReportResultatToFile(aventuriers);
		} catch (IOException e) {
			logger.warn(e.getMessage());
		}
	}

	public final void setAventuriers(final List<Aventurier> aventuriers) {
		this.aventuriers = aventuriers;
	}
	
	public static class Builder {
		private int dimensionCarte;
		private ConcurrentHashMap<String, Position> cases;
		private List<Aventurier> aventuriers;

		private Builder() {
		}

		public static Builder carte() {
			return new Builder();
		}


		public Builder withCases(ConcurrentHashMap<String, Position> cases) {
			this.cases = cases;
			return this;
		}

		public Builder withAventuriers(List<Aventurier> aventuriers) {
			this.aventuriers = aventuriers;
			return this;
		}

		public Carte build() {
			return new Carte(cases, aventuriers);
		}
	}
}
