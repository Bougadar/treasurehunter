package fr.carbonit.treasurehunter.metier.aventurier;


import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.google.common.base.Objects;
import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;
import fr.carbonit.treasurehunter.metier.carte.Carte;
import fr.carbonit.treasurehunter.metier.carte.Position;
import fr.carbonit.treasurehunter.metier.carte.TypePosition;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;

/**
 * Created by bougadar on 26/04/2015.
 */
public class Aventurier implements Serializable {

	private static Logger logger = ((Logger) LoggerFactory.getLogger(Aventurier.class));


	private final String nomAventurier;
	private Orientation orientationCourante;
	private Position positionCourante;
	private int sacDeTresor;
	private final List<Mouvement> sequenceDeplacements;


	private IntFunction ajouterTresor = (int x) -> (this.sacDeTresor + x);


	private Aventurier(Builder builder) {
		logger.setLevel(Level.INFO);
		nomAventurier = builder.nomAventurier;
		orientationCourante = builder.orientationInitiale;
		this.modifierPositionCourante(builder.positioninitiale);
		sequenceDeplacements = builder.sequenceDeplacements;
		this.sacDeTresor = 0;
		logger.info("Aventurier " + nomAventurier + " CREER");

	}

	private void ramasserTresor() {
		sacDeTresor = (int) ajouterTresor.apply(this.positionCourante.getTresor());
	}

	private void setOrientationCourante(Orientation orientationCourante) {
		this.orientationCourante = orientationCourante;
	}

	public int getSacDeTresor() {
		return this.sacDeTresor;
	}

	private final void modifierPositionCourante(Position position) {
		this.positionCourante = position;
	}


	public void executerDeplacement() {
		PeekingIterator<Mouvement> mouvementIterator = Iterators.peekingIterator(sequenceDeplacements.iterator());
		Predicate<Position> isPositionSuivanteTypeMontagne = (Position x) -> {
			return (x.getNatureDeLaPosition() == TypePosition.TRESOR);
		};
		Predicate<Position> isPositionCouranteTypeTresor = (Position x) -> {
			return (x.getNatureDeLaPosition() == TypePosition.TRESOR);
		};
		Predicate<Mouvement> isAvancer = (Mouvement x) -> x == Mouvement.AVANCER;
		Function<Position, Position> avancerSurPositionSuivante = (Position x) -> this.orientationCourante.getPositionSuivante(x);
		logger.info("Début de la Séquence de déplacement de " + nomAventurier);
		logger.info(" il a " + this.sequenceDeplacements.size() + " déplacements à effectuer");


		while (mouvementIterator.hasNext()) {
			Mouvement mouvementAEffectue = mouvementIterator.next();
			if (isAvancer.test(mouvementAEffectue)) {

				if (((this.orientationCourante.getPositionSuivante(this.positionCourante)).getNatureDeLaPosition().equals(TypePosition.MONTAGNE))) {
					logger.info(nomAventurier + " est bloqué par la Montagne en Position " + "(" + avancerSurPositionSuivante.apply((this.positionCourante)) + ")");

					while (mouvementIterator.hasNext() && mouvementIterator.peek().equals(Mouvement.AVANCER)) {
						mouvementIterator.next();

					}

				} else {
					modifierPositionCourante(avancerSurPositionSuivante.apply(this.positionCourante));
					logger.info(nomAventurier + " avance en Position " + "(" + this.positionCourante + ")");
					if (isPositionCouranteTypeTresor.test(this.positionCourante)) {
						ramasserTresor();
						logger.info(nomAventurier + " à ramasser le trésor  en Position " + "(" + (this.positionCourante) + ")");

					}


				}


			} else {
				setOrientationCourante(Orientation.getNouvelleOrientation(this.orientationCourante, mouvementAEffectue::toString));
				logger.info(nomAventurier + " à changer d'orientation, il s'oriente maintenant vers :" + this.orientationCourante.toString());
			}
		}
		logger.info(nomAventurier + " à fini son parcours, ca pos finale est : " + positionCourante);
	}

	public static final class Builder {
		private String nomAventurier;
		private Orientation orientationInitiale;
		private Position positioninitiale;
		private List<Mouvement> sequenceDeplacements;

		public Builder() {
		}

		public Builder withNomAventurier(String nomAventurier) {
			this.nomAventurier = nomAventurier;
			return this;
		}

		public Builder withOrientationInitiale(Orientation orientationInitiale) {
			this.orientationInitiale = orientationInitiale;
			return this;
		}

		public Builder withPositioninitiale(Position positioninitiale) {
			this.positioninitiale = positioninitiale;
			return this;
		}

		public Builder withSequenceDeplacements(List<Mouvement> sequenceDeplacements) {
			this.sequenceDeplacements = sequenceDeplacements;
			return this;
		}

		public Aventurier build() {
			return new Aventurier(this);
		}
			}


	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Aventurier))
			return false;
		Aventurier that = (Aventurier) o;
		return Objects.equal(nomAventurier, that.nomAventurier);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(nomAventurier);
	}

	@Override
	public String toString() {
		return new StringBuffer().append(nomAventurier).append(": ").append(positionCourante).append(" ").append(getSacDeTresor()).toString();
	}
		}


