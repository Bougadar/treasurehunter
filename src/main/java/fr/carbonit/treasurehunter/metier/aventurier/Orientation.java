package fr.carbonit.treasurehunter.metier.aventurier;

import fr.carbonit.treasurehunter.metier.carte.Carte;
import fr.carbonit.treasurehunter.metier.carte.Position;

import java.util.function.Supplier;

/**
 * Created by bougadar on 26/04/2015.
 */
public enum Orientation {
	N(0, ModeDeCalcul.DECREMENTER_LIGNE), E(90, ModeDeCalcul.INCREMENTER_COLONNE), S(180, ModeDeCalcul.INCREMENTER_LIGNE), W(270, ModeDeCalcul.DECREMENTER_COLONNE);
	private final int angleOrientation;

	private final ModeDeCalcul modeDeCalcul;

	Orientation(int angle, ModeDeCalcul modeDeCalcul) {
		this.angleOrientation = angle;
		this.modeDeCalcul = modeDeCalcul;
	}

	Position getPositionSuivante(Position positionCourante) {
		return modeDeCalcul.getPositionSuivante(positionCourante);
	}

	public int getAngleOrientation() {
		return this.angleOrientation;
	}

	public static final Orientation getNouvelleOrientation(Orientation orientationCourante, Supplier<String> nouvelleDirection) {
		Orientation orientation = null;
		int angleOrientation = nouvelleDirection.get().equals(Mouvement.TOURNER_A_DROITE.toString()) ? orientationCourante.getAngleOrientation() + 90 : orientationCourante.getAngleOrientation() - 90;
		switch (angleOrientation) {
			case 0:
			case 360:
				orientation = Orientation.N;
				break;
			case -90:
			case 270:
				orientation = Orientation.W;
				break;
			case 180:
				orientation = Orientation.S;
				break;
			case 90:
				orientation = Orientation.E;


		}
		return orientation;
	}

	private enum ModeDeCalcul {
		INCREMENTER_COLONNE {
			@Override
			Position getPositionSuivante(Position positionCourante) {
				String coord = new Position.Builder().withColonne(positionCourante.getColonne() + 1).withLigne(positionCourante.getLigne()).build().toString();
				return Carte.getPositionReferences(coord);
			}
		}, INCREMENTER_LIGNE {
			@Override
			Position getPositionSuivante(Position positionCourante) {
				String coord = new Position.Builder().withLigne(positionCourante.getLigne() + 1).withColonne(positionCourante.getColonne()).build().toString();
				return Carte.getPositionReferences(coord);
			}
		}, DECREMENTER_COLONNE {
			@Override
			Position getPositionSuivante(Position positionCourante) {
				String coord = new Position.Builder().withColonne(positionCourante.getColonne() - 1).withLigne(positionCourante.getLigne()).build().toString();
				return Carte.getPositionReferences(coord);
			}
		}, DECREMENTER_LIGNE {
			@Override
			Position getPositionSuivante(Position positionCourante) {
				String coord = new Position.Builder().withColonne(positionCourante.getColonne()).withLigne(positionCourante.getLigne() - 1).build().toString();
				return Carte.getPositionReferences(coord);
			}
		};

		abstract Position getPositionSuivante(Position positionCourante);

	}


}
