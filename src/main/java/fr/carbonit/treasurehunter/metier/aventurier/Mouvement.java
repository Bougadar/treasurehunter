package fr.carbonit.treasurehunter.metier.aventurier;

import com.google.common.collect.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by bougadar on 26/04/2015.
 */
public enum Mouvement {
	AVANCER("A"), TOURNER_A_DROITE("D"), TOURNER_A_GAUCHE("G");

	private String mouvement;

	Mouvement(String mouvement) {
		this.mouvement = mouvement;
	}

	@Override
	public String toString() {
		return this.mouvement;
	}

	private static final Map<String, Mouvement> stringToEnum = new HashMap<>();

	public static Mouvement fromString(String deplacement) {
		return stringToEnum.get(deplacement);
	}

	static {
		Stream.of(Mouvement.values()).forEach(x -> stringToEnum.put(x.toString(), x));

	}

	public static List<Mouvement> mouvementBuilder(String sequenceMouvement) {
		List<Mouvement> mouvements = Lists.newArrayList();

		mouvements = sequenceMouvement.chars().mapToObj(i -> (char) i).map(c -> c.toString()).map((s) -> Mouvement.fromString(s)).collect(Collectors.toList());
		return mouvements;
	}
}
