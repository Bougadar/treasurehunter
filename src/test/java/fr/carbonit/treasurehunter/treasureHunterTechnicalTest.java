package fr.carbonit.treasurehunter;

import fr.carbonit.treasurehunter.beanconfig.TreasureHunterSpringBeanCOnfig;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * Created by bougadar on 01/05/2015.
 */

@Test(groups = "METIER", enabled = true)
@ContextConfiguration(classes = TreasureHunterSpringBeanCOnfig.class)
public class treasureHunterTechnicalTest extends AbstractTestNGSpringContextTests {


}
